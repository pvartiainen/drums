import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  drumLoops = [];

  constructor(public router: Router) {
    this.drumLoops = ['Simple', 'Groovy', 'Mambo', 'Discofunk', 'Rock'];
  }

  goToMusicPage(audio:string) {
    this.router.navigateByUrl('music/' + audio);
  }

}
